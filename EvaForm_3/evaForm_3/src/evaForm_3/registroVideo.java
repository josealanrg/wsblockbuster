package evaForm_3;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import net.miginfocom.swing.MigLayout;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.Date;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.awt.event.ActionEvent;
import javax.swing.JTextArea;
import javax.swing.JTable;

public class registroVideo {

	private JFrame frame;
	private JTextField txtCliente;
	private JLabel lblRegistroDeRenta;
	private JLabel lblCliente;
	private JLabel lblMembresia;
	private JTextField txtMembresia;
	private JLabel lblPelicula;
	private JTextField txtPeli;
	private JLabel lblPrecio;
	private JTextField txtPrecio;
	private JLabel lblFolio;
	private JLabel lblFecha;
	private JButton btnNewButton;
	private JTable table;
	private JButton btnGuardar;
	private JLabel lblTipo;
	private JTextField txtTipo;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					registroVideo window = new registroVideo();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public registroVideo() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		Conexion c=new Conexion();
	//	Date fecha = new Date();
		Calendar fecha = new GregorianCalendar();
		 int a�o = fecha.get(Calendar.YEAR);
	        int mes = fecha.get(Calendar.MONTH);
	        int dia = fecha.get(Calendar.DAY_OF_MONTH);
		
		frame = new JFrame();
		frame.setBounds(100, 100, 489, 364);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new MigLayout("", "[45px,grow][86px,grow][62px][86px,grow]", "[20px][][][][][][][][][grow][][grow][][][][]"));
		
		lblRegistroDeRenta = new JLabel("REGISTRO DE RENTA DE VIDEOS");
		lblRegistroDeRenta.setFont(new Font("Arial", Font.BOLD, 13));
		frame.getContentPane().add(lblRegistroDeRenta, "cell 1 0,alignx center,aligny center");
		
		lblFecha = new JLabel("FECHA");
		lblFecha.setFont(new Font("Tahoma", Font.BOLD, 12));
		frame.getContentPane().add(lblFecha, "cell 3 1,alignx center,aligny center");
		 lblFecha.setText(" "+dia+ "/"+ mes+"/" + a�o);
		lblFolio = new JLabel("");
		lblFolio.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblFolio.setText("FOLIO: "+c.ObtenerIdRenta());
		frame.getContentPane().add(lblFolio, "cell 0 2,alignx center");
		

		lblCliente = new JLabel("CLIENTE");
		lblCliente.setFont(new Font("Tahoma", Font.BOLD, 12));
		frame.getContentPane().add(lblCliente, "cell 0 4,alignx trailing");
		
		txtCliente = new JTextField();
		frame.getContentPane().add(txtCliente, "cell 1 4,growx");
		txtCliente.setColumns(10);
		
		lblMembresia = new JLabel("MEMBRESIA");
		lblMembresia.setFont(new Font("Tahoma", Font.BOLD, 12));
		frame.getContentPane().add(lblMembresia, "cell 2 4,alignx trailing");
		
		txtMembresia = new JTextField();
		frame.getContentPane().add(txtMembresia, "cell 3 4,growx");
		txtMembresia.setColumns(10);
		
		lblPrecio = new JLabel("PRECIO");
		lblPrecio.setFont(new Font("Tahoma", Font.BOLD, 12));
		frame.getContentPane().add(lblPrecio, "cell 3 6,alignx center,aligny center");
		
		lblPelicula = new JLabel("PELICULA:");
		lblPelicula.setFont(new Font("Tahoma", Font.BOLD, 12));
		frame.getContentPane().add(lblPelicula, "cell 0 7,alignx trailing");
		
		txtPeli = new JTextField();
		frame.getContentPane().add(txtPeli, "cell 1 7,growx");
		txtPeli.setColumns(10);
		
		txtPrecio = new JTextField();
		frame.getContentPane().add(txtPrecio, "cell 3 7,growx");
		txtPrecio.setColumns(10);
		btnNewButton = new JButton("Hisorial...");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
						table.setModel(c.muestraTabla("vw_DetalleRenta"));
					//	textArea.setText(c.Consultar("select*from renta"));
					
							
			}
		});
		
		btnGuardar = new JButton("GUARDAR");
		btnGuardar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				c.Insertar("insert into cliente values ('"+txtCliente.getText()+"','"+txtMembresia.getText()+"')");
				c.Insertar("insert into video values ('"+ txtPeli.getText() +"','"+txtTipo.getText()+"')");
				//obtiene id cliente
				int idCliente=c.ObtenerIdCliente();
				//obtiene id video
				int idVideo=c.ObtenerIdVideo();
				//inserta en renta
				c.Insertar("insert into renta values (GETDATE(),"+idVideo+","+idCliente+","+txtPrecio.getText()+")");
				
				txtCliente.setText("");
				txtMembresia.setText("");
				txtPeli.setText("");
				txtTipo.setText("");
				txtPrecio.setText("");
			}
		});
		
		lblTipo = new JLabel("TIPO:");
		lblTipo.setFont(new Font("Tahoma", Font.BOLD, 12));
		frame.getContentPane().add(lblTipo, "cell 0 8,alignx trailing,aligny center");
		
		txtTipo = new JTextField();
		frame.getContentPane().add(txtTipo, "cell 1 8,growx");
		txtTipo.setColumns(10);
		frame.getContentPane().add(btnGuardar, "cell 2 10");
		
		table = new JTable();
	
		frame.getContentPane().add(table, "cell 1 11,grow");
		btnNewButton.setFont(new Font("Arial", Font.BOLD, 12));
		frame.getContentPane().add(btnNewButton, "cell 2 12");
	}

}
